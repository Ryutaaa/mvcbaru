<div class="container mt-5">
    <div class="card" style="width: 18rem;">
        <div class="card-body">
            <h5 class="card-title"><?= $data['blg']['penulis']; ?></h5>
            <h6 class="card-subtitle mb-2 text-muted"><?= $data['blg']['buku']; ?></h6>

            <p class="card-text"><?= $data['blg']['tulisan']; ?></p>

            <a href="<?= BASEURL; ?>/blog" class="card-link btn btn-primary">Back</a>

        </div>
    </div>
</div>