<div class="container mt-4">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#formModal">
                Tambah Data Blog
            </button>
            <h3 class="mt-3">Daftar Blog</h3>
            <ul class="list-group">
                <?php foreach ($data['blg'] as $blg) :  ?>
                    <li class="list-group-item">
                        <?= $blg['penulis']; ?>
                        <a href="<?= BASEURL; ?> /mahasiswa/hapus/<?= $blg['id']; ?>" class="badge text-bg-danger float-end ms-2" onclick="return confirm('yakin mau hapus?')">Delete</a>
                        <a href="<?= BASEURL; ?> /mahasiswa/ubah/<?= $blg['id']; ?>" class="badge text-bg-warning float-end ms-2 tampilModalUbah" data-bs-toggle="modal" data-bs-target="#formModal">Edit</a>
                        <a href="<?= BASEURL; ?> /mahasiswa/detail/<?= $blg['id']; ?>" class="badge text-bg-primary float-end ms-2">Detail</a>
                    </li>
                <?php endforeach; ?>

            </ul>



        </div>
    </div>
</div>