<?php
class About extends Controller
{
    public function index($nama = 'Ryutaaa desu', $pekerjaan = 'Mahasiswa S3 Programer Japan Internasional', $qts = '"Kamu mungkin tau tentang dirinya,tapi kau tidak tau apa apa tentang perasaannya."')
    {
        $data['judul'] = 'About';
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['qts'] = $qts;
        $this->view('templates/header', $data);
        $this->view('about/index', $data);
        $this->view('templates/footer');
    }
    public function page()
    {
        $data['judul'] = 'Page';
        $this->view('templates/header', $data);
        $this->view('about/page');
        $this->view('template/footer');
    }
}
