<?php
class Blog extends Controller
{
    public function index()
    {
        $data['judul'] = ' Daftar Blog';
        $data['blg'] = $this->model('Blog_model')->getAllBlog();
        $this->view('templates/header', $data);
        $this->view('blog/index', $data);
        $this->view('templates/footer');
    }
    public function detail($id)
    {
        $data['judul'] = 'Detail Blog';
        $data['blg'] = $this->model('Blog_model')->getBlogById($id);
        $this->view('templates/header', $data);
        $this->view('blog/detail', $data);
        $this->view('templates/footer');
    }
}
